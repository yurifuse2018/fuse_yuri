<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板システム</title>
        <link href="./css/style3.css" rel="stylesheet" type="text/css">
        <title>掲示板システム</title>
    </head>
    <body>
         <div class="header">
        	<div class="menu">
        	<a href="./">ホーム</a>
         	<a href="newMessage">新規投稿</a>
          	<c:if test="${ isShowMenuForm==true }">
          	<a href="userSetting">ユーザー管理画面</a>
          	<a href="signup">ユーザー登録</a>
           	</c:if>
           	<a href="logout">ログアウト</a>
           	</div>
                <h2>新規投稿画面</h2>
          </div>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="form-area">
        		<form action="newMessage" method="post">
        		<p>件名</p>
        		<input type="text" name="title" id="title" value="${title}"><br />
            	<p>内容</p>
            	<textarea name="content" cols="100" rows="5" class="tweet-box"><c:out value="${content}"/></textarea>
            	<br />
            	<p>カテゴリー</p>
        		<input type="text" name="category" id="category" value="${category}">
        		<br />
            	<input type="submit" value="投稿" id="submit">
        		</form>
			</div>

			 <div class ="return">
            	<a href="./">戻る</a>
            </div>
            <div class="copyright"> Copyright(c)Yuri Fuse</div>
        </div>
    </body>
</html>