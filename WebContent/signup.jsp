<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style2.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
    </head>
    <body>
		<div class="header">
       	<div class="menu">
        	<a href="./">ホーム</a>
         	<a href="newMessage">新規投稿</a>
          	<c:if test="${ isShowMenuForm==true }">
          	<a href="userSetting">ユーザー管理画面</a>
          	<a href="signup">ユーザー登録</a>
           	</c:if>
           	<a href="logout">ログアウト</a>
      	</div>
        <h2>ユーザー登録画面</h2>
        </div>
          <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                        <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

        <div class="main-contents">
            <form action="signup" method="post">
                <label for="account">アカウント名</label>
                <input name="account" id="account" value="${account}" /> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="conPassword">確認用パスワード</label>
                <input name="conPassword" type="password" id="conPassword" /> <br />

                <label for="name">名称</label>
                <input name="name" id="name" value="${name}" /><br />

                <label for="branch">支店</label>
                <select name="branch_id" id="branch">
                  <option value="4" <c:if test="${branchId==4}">selected</c:if>>本社</option>
                  <option value="1" <c:if test="${branchId==1}">selected</c:if>>支店A</option>
                  <option value="2" <c:if test="${branchId==2}">selected</c:if>>支店B</option>
                  <option value="3" <c:if test="${branchId==3}">selected</c:if>>支店C</option>
                </select>
                <br />

                <label for="department">部署・役職</label>
                <select name="department_id" id="department">
                  <option value="1" <c:if test="${departmentId==1}">selected</c:if>>総務人事</option>
                  <option value="2" <c:if test="${departmentId==2}">selected</c:if>>情報管理</option>
                  <option value="3" <c:if test="${departmentId==3}">selected</c:if>>支店長</option>
                  <option value="4" <c:if test="${departmentId==4}">selected</c:if>>社員</option>
                </select>
                <br />
                <input type="hidden" name="stop" value="1">
                <input type="submit" value="登録" id="submit"/> <br />
            </form>
        </div>

        <div class="footer">
        <a href="./"> 戻る</a>
        <div class="copyright">Copyright(c)Your Name</div>
        </div>
    </body>
</html>