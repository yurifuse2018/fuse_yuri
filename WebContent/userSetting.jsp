<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <script type="text/javascript">
			function check(){
				if(window.confirm('変更してよろしいですか？')){ // 確認ダイアログを表示
					return true;
				}
				else{
					return false; // 送信を中止
				}
			}
		</script>
		<link href="./css/style4.css" rel="stylesheet" type="text/css">
    </head>

    <body>
         <div class="header">
        	<div class="menu">
        	<a href="./">ホーム</a>
         	<a href="newMessage">新規投稿</a>
          	<c:if test="${ isShowMenuForm==true }">
          	<a href="userSetting">ユーザー管理画面</a>
          	<a href="signup">ユーザー登録</a>
           	</c:if>
           	<a href="logout">ログアウト</a>
           	</div>
          	<h2 class="title">掲示板ホーム画面</h2>
         </div>
        <div class="main-contents">
    		<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

			<div class="user">
				<div class="userLists">
          			 <table border=1 width="800">
			         <tr>
          			  <th>ID</th>
          			  <th>アカウント名</th>
          			  <th>名称</th>
          			  <th>支店</th>
          			  <th>部署・役職</th>
          			  <th>復活・停止</th>
          			  <th>ステータス</th>
          			  <th>編集</th>
          			 </tr>
    			 <c:forEach items="${userLists}" var="userList">
          			 <tr align=center>
                		<td><div class="id"><c:out value="${userList.id}" /></div></td>
                		<td><div class="account"><c:out value="${userList.account}" /></div></td>
                		<td><div class="name"><c:out value="${userList.name}" /></div></td>
                		<td><div class="branch"><c:out value="${userList.branch}" /></div></td>
                		<td><div class="department"><c:out value="${userList.department}" /></div></td>

                		<td>
                		<c:if test="${!(userList.id==loginUser.id)}">
                		<div class="isStopped">
                		<form action="stopUser" method="post" onSubmit="return check()">
                		    <c:if test="${userList.isStopped==1}">
                			<input type="submit" value="停止">
                			<input type="hidden" name="stop" value="0">
                			<input type="hidden" name="id" value="${userList.id}">
                			</c:if>

                			<c:if test="${userList.isStopped==0}">
                			<input type="submit" value="復活">
                			<input type="hidden" name="stop" value="1">
                			<input type="hidden" name="id" value="${userList.id}">
                			</c:if>
                		</form>
                		</div>
                		</c:if>
                		</td>

                		<td><div class="status">
            			<c:if test="${userList.isStopped==0}"><c:out value="停止中" /></c:if>
            			<c:if test="${userList.isStopped==1}"><c:out value="使用中" /></c:if>
                		</div></td>

                		<td><div class="edit">
                		<form action="editUser" method="get">
                		 <input type="submit" value="編集" >
                		 <input type="hidden" name="edit_id" value="${userList.id}">
          			    </form>
                		</div></td>
                	</tr>
    			 </c:forEach>
                </table>
                </div>
    	 	</div>
    		<a href="./" class="return">ホームに戻る</a>
            <div class="copyright"> Copyright(c)Yuri Fuse</div>
    	 </div>
</body>
</html>