<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ page import="java.util.Date, java.text.DateFormat" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>掲示板システム</title>

        <script type="text/javascript">
			function check(){
				if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示
					return true;
				}// 「OK」時は送信を実行
				else{ // 「キャンセル」時の処理
					return false; // 送信を中止
				}
			}
		</script>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
       	<div class="header">
        	<div class="menu">
        	<a href="./">ホーム</a>
         	<a href="newMessage">新規投稿</a>
          	<c:if test="${ isShowMenuForm==true }">
          	<a href="userSetting">ユーザー管理画面</a>
          	<a href="signup">ユーザー登録</a>
           	</c:if>
           	<a href="logout">ログアウト</a>
           	</div>
          	<h2 class="title">掲示板ホーム画面</h2>
         </div>

        <div class="main-contents">

			<div class="posts">
		            <c:if test="${ not empty errorMessages }">
		                <div class="errorMessages">
		                    <ul>
		                        <c:forEach items="${errorMessages}" var="message">
		                            <li><c:out value="${message}" />
		                        </c:forEach>
		                    </ul>
		                </div>
		                <c:remove var="errorMessages" scope="session"/>
		            </c:if>

			<div class="narrowDownForm">
			<form action="index.jsp" method="get">
			<div class="date">
			<label for="sdate">開始</label>
			<input type="date" name ="sdate" value="${sDate}"></input>
			</div>

			<div class="date">
			<label for="edate">終了</label>
			<input type="date" name ="edate" value="${eDate}" >
			</div>
			<div class="date">
			<label for="category">カテゴリー検索</label>
			<input type="text" name ="category" value="${category}">
			<input type="submit" value="検索">
			</div>
			</form>
			<br/>
			<div class="count">
			<c:out value="全${counts}件"></c:out>
			</div>
			</div>

    			 <c:forEach items="${messages}" var="message">
    			 <div class="post-wrapper">
           			 <div class="message">
                		<h3 class="title"><c:out value="${message.title}" /></h3>
                		<span class="name">《 投稿者：<c:out value="${message.name}" /> 》</span>
                		<div class="content"><pre><c:out value="${message.content}" /></pre></div>
                		<div class="category">カテゴリー:<p><c:out value="${message.category}" /></p></div>
                		<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>


                		<c:if test="${ message.userId == loginUser.id}">
	                		<div class="deleteMessage">
	                		<form action="deleteMessage" method="post" onSubmit="return check()">
	                		<input type = "submit" value="投稿を削除する" ><br>
	                		<input type="hidden" name="message_id" value="${message.id}">
	                		</form>
	                		</div>
                		</c:if>
                		</div>

            	<div class="comments">
            	<c:forEach items="${comments}" var="comment">

            	<c:if test="${comment.postId == message.id}">
            			<div class="comment">
            			<div class="content"><pre><c:out value="${comment.content}" /></pre></div>
            			<div class="date"><c:out value="${comment.created_date}" /></div>
            			<div class="name">《 投稿者：<c:out value="${comment.name}" />》</div>
                  	<c:if test="${ comment.userId == loginUser.id}">
                		<div class="deleteComment">
                		<form action="deleteComment" method="post" onSubmit="return check()">
                		<input type="submit" value="コメントの削除" >
                		<input type="hidden" name ="comment_id" value="${comment.id}">
                		</form>
                		</div>
               	  	</c:if>
               	</div>
            	</c:if>
            	</c:forEach>
            	</div>

            		<div class="newCommentForm">
        				<form action="newComment" method="post">
            			【この投稿にコメントする】
            			<textarea name="content" cols="100" rows="5" class="tweet-box">
            				<c:if test="${ not empty comment }">
            					<c:out value="${comment}"></c:out>
            					<c:remove var="comment" scope="session"/>
		            		</c:if>
            			</textarea>
           				<input type="hidden" name ="post_id" value="${message.id}">
           				<input type="hidden" name ="user_id" value="${user.id}">
           				<div class="submitButton">
           				<input type="submit" id="submit" value="コメント投稿"/>
           				</div>
        				</form>
        			</div>
        			</div>
   				 </c:forEach>
			</div>
            <a href="./" class="return">ホームに戻る</a>
            <div class="copyright"> Copyright(c)Yuri Fuse</div>

        </div>
    </body>

</html>