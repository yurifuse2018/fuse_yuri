package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.EditUserService;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/editUser" })
 public class EditUserServlet extends HttpServlet {
    @Override
   protected void doGet(HttpServletRequest request,
           HttpServletResponse response) throws IOException, ServletException {


	   HttpSession session = request.getSession();


	   	if(request.getParameter("edit_id")==null) {
	   		response.sendRedirect("userSetting");
	   	}else if(!request.getParameter("edit_id").matches("[0-9]+")) {
	   		String messages = "存在しないアカウントです。";
	   		session.setAttribute("errorMessages", messages);
	   		response.sendRedirect("userSetting");
	   		//request.getRequestDispatcher("/userSetting").forward(request, response);
	   	}else {
	   	   int userId = Integer.parseInt(request.getParameter("edit_id"));
	   	   User editUser = new EditUserService().getEditUser(userId);

	       if(editUser==null) {
		   		String messages = "存在しないアカウントです。";
		   		session.setAttribute("errorMessages", messages);
		   		response.sendRedirect("userSetting");
   	       		//request.getRequestDispatcher("userSetting.jsp").forward(request, response);
   	   		}else {
   	       		request.setAttribute("editUser", editUser);
	   	        User user = (User) request.getSession().getAttribute("loginUser");
	   	        int userDepartment = user.getDepartmentId();

	   	    	boolean isShowMenuForm;
	   	        if (userDepartment == 1) {
	   	        	isShowMenuForm = true;
	   	        } else {
	   	        	isShowMenuForm = false;
	   	        }
	   	        request.setAttribute("isShowMenuForm", isShowMenuForm);
   	       		request.getRequestDispatcher("userEdit.jsp").forward(request, response);
   	   		}
	   	}
   }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {
        	int userId =  Integer.parseInt(request.getParameter("id"));
        	String userName = request.getParameter("name");
        	String userAccount = request.getParameter("account");
        	String userPassword = request.getParameter("password");
        	int userBranch = Integer.parseInt(request.getParameter("branch_id"));
        	int userDepartment = Integer.parseInt(request.getParameter("department_id"));

        	User editUser = new User();
        	editUser.setId(userId);
        	editUser.setName(userName);
        	editUser.setAccount(userAccount);
        	editUser.setPassword(userPassword);
        	editUser.setBranchId(userBranch);
        	editUser.setDepartmentId(userDepartment);

        	new EditUserService().update(editUser);

        	response.sendRedirect("userSetting");

        }else {
            session.setAttribute("errorMessages", messages);

        	int userId =  Integer.parseInt(request.getParameter("id"));
        	User editUser = new EditUserService().getEditUser(userId);
        	request.setAttribute("editUser", editUser);

            request.getRequestDispatcher("userEdit.jsp").forward(request, response);
        }
    }

        private boolean isValid(HttpServletRequest request, List<String> messages) {

        	int userId =  Integer.parseInt(request.getParameter("id"));
            String account = request.getParameter("account");
            String name = request.getParameter("name");
            String password = request.getParameter("password");
            String confirmPass = request.getParameter("conPassword");
        	int userBranch = Integer.parseInt(request.getParameter("branch_id"));
        	int userDepartment = Integer.parseInt(request.getParameter("department_id"));


            if (StringUtils.isBlank(account) == true) {
                messages.add("アカウント名を入力してください");
            }else if (account.length()<6 || account.length()>20 ) {
	            messages.add("「アカウント名」は6文字以上20文字以下で入力してください");
            }else if (account.matches("[\\W]+")) {
	        	messages.add("「アカウント」は半角英数字で入力してください");
	        }
            System.out.println(password);

            if(!StringUtils.isEmpty(password)  ==true) {
            	if(StringUtils.isBlank(password)) {
            		messages.add("「パスワード」は半角英数字で入力してください");
            	}else if (password.length()<6 || password.length()>20 ) {
		            messages.add("「パスワード」は6文字以上20文字以下で入力してください");
	            }
            }

            if (StringUtils.isBlank(name) == true) {
                messages.add("名称を入力してください");
            }else if (name.length()>11) {
	            messages.add("「名称」は10文字以下で入力してください");
	        }

            if(userBranch==4) {
            	if((userDepartment != 1 && userDepartment !=2) == true) {
            		messages.add("支店と部署の組み合わせが不正です。");
            	}
            }
            if(userDepartment == 1) {
            	if(userBranch!=4) {
            		messages.add("支店と部署の組み合わせが不正です。");
            	}
            }
            if(userDepartment == 2) {
            	if(userBranch!=4) {
            		messages.add("支店と部署の組み合わせが不正です。");
            	}
            }
	        if(!(password.equals(confirmPass))) {
	        	messages.add("パスワードが一致しません。");
	        }

            // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
            boolean duplicateCheck =new UserService().getAccountList(account,userId);
            if (duplicateCheck== false) {
                messages.add("このアカウント名は使われています。");
            }

            if (messages.size() == 0) {
                return true;
            } else {
                return false;
            }
        }

}