package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;
import chapter7.beans.UserComment;
import chapter7.beans.UserMessage;
import chapter7.service.CommentService;
import chapter7.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
        User user = (User) request.getSession().getAttribute("loginUser");
        int userDepartment = user.getDepartmentId();

        User loginUser = (User) session.getAttribute("loginUser");
        request.setAttribute("loginUser", loginUser);

    	boolean isShowMenuForm;
        if (userDepartment == 1) {
        	isShowMenuForm = true;
        } else {
        	isShowMenuForm = false;
        }
        request.setAttribute("isShowMenuForm", isShowMenuForm);

    	String category = request.getParameter("category");
    	String sDate =request.getParameter("sdate");
    	String eDate =request.getParameter("edate");

    	request.setAttribute("sDate",sDate);
    	request.setAttribute("eDate",eDate);
    	request.setAttribute("category",category);

        List<UserMessage> messages = new MessageService().getMessage(sDate,eDate,category);
        int count = messages.size();

        request.setAttribute("messages", messages);
        request.setAttribute("counts", count);


        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);


        request.getRequestDispatcher("top.jsp").forward(request, response);
    }
}