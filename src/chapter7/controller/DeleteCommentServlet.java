package chapter7.controller;

import static chapter7.utils.CloseableUtil.close;

import java.io.IOException;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.service.DeleteCommentService;


 @WebServlet(urlPatterns = { "/deleteComment" })
 public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	int id = Integer.parseInt(request.getParameter("comment_id"));

    	new DeleteCommentService().register(id);

    	response.sendRedirect("./");
    	}
}