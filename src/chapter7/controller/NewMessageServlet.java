package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Message;
import chapter7.beans.User;
import chapter7.service.MessageService;


@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");
        int userDepartment = user.getDepartmentId();

    	boolean isShowMenuForm;
        if (userDepartment == 1) {
        	isShowMenuForm = true;
        } else {
        	isShowMenuForm = false;
        }
        request.setAttribute("isShowMenuForm", isShowMenuForm);

    	request.getRequestDispatcher("post.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {
            User user = (User) session.getAttribute("loginUser");

            String content = request.getParameter("content");

            Message message = new Message();
            message.setTitle(request.getParameter("title"));
            message.setContent(content);
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            String title = request.getParameter("title");
            String content = request.getParameter("content");

            String category = request.getParameter("category");

            request.setAttribute("title",title);
            request.setAttribute("content",content);
            request.setAttribute("category",category);
            //response.sendRedirect("./");
            request.getRequestDispatcher("post.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String postedTitle = request.getParameter("title");
        if (StringUtils.isBlank(postedTitle) == true) {
            messages.add("件名を入力してください");
        }else if (30 < postedTitle.length()) {
            messages.add("件名を30文字以下で入力してください");
        }

    	String postedContent = request.getParameter("content");
        if (StringUtils.isBlank(postedContent) == true) {
            messages.add("内容を入力してください");
        }
        if (1000 < postedContent.length()) {
            messages.add("内容を1000文字以下で入力してください");
        }

        String postedCategory = request.getParameter("category");
        if (StringUtils.isBlank(postedCategory) == true) {
            messages.add("カテゴリー名を入力してください");
        }
        if (10 < postedCategory.length()) {
            messages.add("カテゴリー名を10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}