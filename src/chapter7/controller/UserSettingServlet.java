package chapter7.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;
import chapter7.beans.UserList;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/userSetting" })
public class UserSettingServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        request.setAttribute("loginUser", loginUser);

        List<UserList> userLists = new UserService().getUserList();
        request.setAttribute("userLists", userLists);

        int userDepartment = loginUser.getDepartmentId();

    	boolean isShowMenuForm;
        if (userDepartment == 1) {
        	isShowMenuForm = true;
        } else {
        	isShowMenuForm = false;
        }
        request.setAttribute("isShowMenuForm", isShowMenuForm);

        request.getRequestDispatcher("userSetting.jsp").forward(request, response);
    }

}


