package chapter7.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.service.EditUserService;

@WebServlet(urlPatterns = { "/stopUser" })
public class StopUserServlet extends HttpServlet {
   @Override
   protected void doGet(HttpServletRequest request,
           HttpServletResponse response) throws IOException, ServletException {

	   request.getRequestDispatcher("editSetting.jsp").forward(request, response);
   }

   @Override
   protected void doPost(HttpServletRequest request,
           HttpServletResponse response) throws IOException, ServletException {

	   int id = Integer.parseInt(request.getParameter("id"));
	   int stop = Integer.parseInt(request.getParameter("stop"));


	   new EditUserService().stopUser(id,stop);

	   response.sendRedirect("userSetting");
   }
}