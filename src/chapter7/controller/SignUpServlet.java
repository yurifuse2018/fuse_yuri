package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.User;
import chapter7.service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        int userDepartment = user.getDepartmentId();

    	boolean isShowMenuForm;
        if (userDepartment == 1) {
        	isShowMenuForm = true;
        } else {
        	isShowMenuForm = false;
        }
        request.setAttribute("isShowMenuForm", isShowMenuForm);
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.setCharacterEncoding("UTF-8");
    	List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        User user = new User();
        if (isValid(request, messages) == true) {
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
            user.setIsStopped(Integer.parseInt(request.getParameter("stop")));

            new UserService().register(user);
            response.sendRedirect("userSetting");
        } else {
        	session.setAttribute("errorMessages", messages);

        	String account = request.getParameter("account");
        	String name = request.getParameter("name");
        	int departmentId = Integer.parseInt(request.getParameter("department_id"));
        	int branchId = Integer.parseInt(request.getParameter("branch_id"));

        	request.setAttribute("account", account);
        	request.setAttribute("name", name);
        	request.setAttribute("departmentId", departmentId);
        	request.setAttribute("branchId", branchId);
        	request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

	        String account = request.getParameter("account");
	        String name = request.getParameter("name");
	        String password = request.getParameter("password");
	        String confirmPass = request.getParameter("conPassword");
	        int branchId = Integer.parseInt(request.getParameter("branch_id"));
	        int departmentId = Integer.parseInt(request.getParameter("department_id"));

	        if (StringUtils.isBlank(account)) {
	            messages.add("「アカウント名」を入力してください");
	        }else if (account.length()<6 || account.length()>20 ) {
	            messages.add("「アカウント名」は6文字以上20文字以下で入力してください");
	        }else if (account.matches("[\\W]+")) {
	        	messages.add("「アカウント」は半角英数字で入力してください");
	        }

	        if (StringUtils.isBlank(name) == true) {
	            messages.add("名称を入力してください");
	        }
	        if (name.length()>10) {
	            messages.add("「名称」は10文字以下で入力してください");
	        }
	        if (StringUtils.isBlank(password) == true) {
	            messages.add("パスワードを入力してください");
	        }else if (password.length()<6 || password.length()>20) {
	            messages.add("「パスワード」は6文字以上20文字以下で入力してください");
	        }
	        if(!(password.equals(confirmPass))) {
	        	messages.add("パスワードが一致しません。");
	        }


        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        boolean duplicateCheck =new UserService().getAccountList(account);
        	if (duplicateCheck== false) {
        		messages.add("このアカウント名は使われています。");
        	}
        	if(branchId==4) {
        		if((departmentId != 1 && departmentId !=2) == true) {
        			messages.add("支店と部署の組み合わせが不正です。");
        		}
        	}
	        if(departmentId == 1) {
	        	if(branchId!=4) {
	        		messages.add("支店と部署の組み合わせが不正です。");
	        	}
	        }
	        if(departmentId == 2) {
	        	if(branchId!=4) {
	        		messages.add("支店と部署の組み合わせが不正です。");
	        	}
	        }

	        if (messages.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
    }


}
