/**
 *
 */
package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;
import chapter7.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(account, password);

        List<String> messages = new ArrayList<String>();
	        if (user!=null) {
	        	int isStopped = user.getIsStopped();
	          if (isStopped!=1) {
	        	  messages.add("ログインに失敗しました。");
	        	  session.setAttribute("errorMessages", messages);
	        	  session.setAttribute("account",account);
	        	  session.setAttribute("password",password);
	        	  request.getRequestDispatcher("login.jsp").forward(request, response);
	        	  return;
	          }else {
	    	  session.setAttribute("loginUser", user);
	    	  response.sendRedirect("./");
	    	  return;
	          }
	        }else {
	        	messages.add("ログインIDまたはパスワードが誤っています");
	        	session.setAttribute("errorMessages", messages);
	        	session.setAttribute("account",account);
	        	session.setAttribute("password",password);
	        	request.getRequestDispatcher("login.jsp").forward(request, response);
	        	return;
	        }
        }
    }
