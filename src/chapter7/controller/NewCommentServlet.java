package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Comment;
import chapter7.beans.User;
import chapter7.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {
            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setContent(request.getParameter("content"));
            comment.setUserId(user.getId());
            comment.setName(request.getParameter("user_name"));
            comment.setPostId(Integer.parseInt(request.getParameter("post_id")));

            new CommentService().register(comment);
            response.sendRedirect("./");
        } else {
        	String comment= request.getParameter("content");
        	session.setAttribute("errorMessages", messages);
        	session.setAttribute("comment",comment);
        	response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String postedContent = request.getParameter("content");

        if (StringUtils.isBlank(postedContent) == true) {
            messages.add("内容を入力してください");

        }else if (500 < postedContent.length()) {
            messages.add("内容を500文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}