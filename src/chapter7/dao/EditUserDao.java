package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.SQLRuntimeException;

public class EditUserDao {

	//編集前のユーザー情報を編集画面に表示
	 public User getUserList(Connection connection,int userId) {
        PreparedStatement ps = null;
        try {
//        	String sql = "SELECT account, name, branch_id, department_id FROM users WHERE id = ?";

        	 StringBuilder sql = new StringBuilder();
        	 sql.append("SELECT * ");
             sql.append("FROM users ");
             sql.append("WHERE id = ?");

        	 ps = connection.prepareStatement(sql.toString());

        	 ps.setInt(1,userId);
        	 ResultSet rs = ps.executeQuery();
        	 List<User> ret = toUserList(rs);

        	 if (ret.isEmpty() == true) {
                 return null;
             } else if (2 <= ret.size()) {
                 throw new IllegalStateException("2 <= userList.size()");
             } else {
                 return ret.get(0);
             }

        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
        	close(ps);
        }
	}

	  private List<User> toUserList(ResultSet rs)throws SQLException {

	    	List<User> editList = new ArrayList<User>();
	    	try {
	    		while(rs.next()){
	    			int id = rs.getInt("id");
	    			String account = rs.getString("account");
	    			String name = rs.getString("name");
	    			int branchId = rs.getInt("branch_id");
	    			int departmentId = rs.getInt("department_id");

	    			User user = new User();

	    			user.setId(id);
	    			user.setAccount(account);
	    			user.setName(name);
	    			user.setBranchId(branchId);
	    			user.setDepartmentId(departmentId);

	    			editList.add(user);
	    		}
	    		return editList;

	    	}finally {
	    		close(rs);
	    	}
	    }




	  //編集したユーザー情報をUPDATEする
	  public void update(Connection connection,User editUser) {

		  PreparedStatement ps = null;
		  try {
			  StringBuilder sql = new StringBuilder();

			  sql.append("UPDATE users SET ");
			  sql.append("account= ? ");
			  sql.append(", name= ? ");
			  if(editUser.getPassword() != null) {
			  sql.append(", password = ? ");
			  }
			  sql.append(", branch_id = ? ");
			  sql.append(", department_id = ? ");
			  sql.append(" WHERE ");
			  sql.append(" id = ?");

			  ps = connection.prepareStatement(sql.toString());

			  if(editUser.getPassword() != null) {
				  ps.setString(1, editUser.getAccount());
				  ps.setString(2, editUser.getName());
				  ps.setString(3, editUser.getPassword());
				  ps.setInt(4, editUser.getBranchId());
				  ps.setInt(5, editUser.getDepartmentId());
				  ps.setInt(6, editUser.getId());
			  }else {
				  ps.setString(1, editUser.getAccount());
				  ps.setString(2, editUser.getName());
				  ps.setInt(3, editUser.getBranchId());
				  ps.setInt(4, editUser.getDepartmentId());
				  ps.setInt(5, editUser.getId());
			  }
//	          int count = ps.executeUpdate();
//	          if (count == 0) {
//	                throw new NoRowsUpdatedRuntimeException();
//	          }

			  System.out.println(ps.toString());
	          ps.executeUpdate();

		  }catch(SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	  }


	  //停止復活機能をUPDATEする
	  public void stopUser(Connection connection,int id, int stop) {

		  PreparedStatement ps = null;

		  try {
			  StringBuilder sql = new StringBuilder();

			  sql.append("UPDATE users SET ");
			  sql.append("is_stoped = ? ");
			  sql.append("WHERE ");
			  sql.append("id = ?");

			  ps = connection.prepareStatement(sql.toString());

			  ps.setInt(1,stop);
			  ps.setInt(2,id);

			  System.out.println(ps.toString());

//	          int count = ps.executeUpdate();
//	          if (count == 0) {
//	                throw new NoRowsUpdatedRuntimeException();
//	          }
	          ps.executeUpdate();
		  }catch(SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	  }
}
