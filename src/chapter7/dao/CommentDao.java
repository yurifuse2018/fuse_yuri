package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Comment;
import chapter7.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("content");
            sql.append(", user_id");
            sql.append(", post_id");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // content
            sql.append(", ?"); // user_id
            sql.append(", ?"); // post_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, comment.getContent());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getPostId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}