package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserMessage;
import chapter7.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num,String sDate, String eDate,String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title, ");
            sql.append("posts.content as content, ");
            sql.append("posts.category as category, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date BETWEEN ? AND ? ");
            if(category!=null&& category!="") {
            sql.append("AND posts.category LIKE ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1,sDate+" 00:00:00");
            ps.setString(2,eDate+" 22:00:00");

            if(category!=null&& category!="") {
            ps.setString(3,"%"+category+"%");
            }

            System.out.println(ps);


            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);

        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String title = rs.getString("title");
                String content = rs.getString("content");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setName(name);
                message.setId(id);
                message.setUserId(userId);
                message.setTitle(title);
                message.setContent(content);
                message.setCategory(category);
                message.setCreated_date(createdDate);

                ret.add(message);

                System.out.println(createdDate);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
