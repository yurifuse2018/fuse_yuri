package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserList;
import chapter7.exception.SQLRuntimeException;

public class UserSettingDao {

	public List<UserList> getUserList(Connection connection,int num) {

		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("branches.id as branch_id, ");
            sql.append("branches.name as branch_name, ");
            sql.append("departments.id as department_id, ");
            sql.append("departments.name as department_name, ");
            sql.append("users.is_stoped as isStopped, ");
            sql.append("users.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps.toString());

            ResultSet rs = ps.executeQuery();
            List<UserList> ret = toUserList(rs);
            return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
        } finally {
            close(ps);
		}
	}

    private List<UserList> toUserList(ResultSet rs)
            throws SQLException {

        List<UserList> ret = new ArrayList<UserList>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                String branch = rs.getString("branch_name");
                int departmentId = rs.getInt("department_id");
                String department = rs.getString("department_name");
                int isStopped = rs.getInt("isStopped");
                //Timestamp createdDate = rs.getTimestamp("created_date");

                UserList userList = new UserList();
                userList.setId(id);
                userList.setAccount(account);
                userList.setName(name);
                userList.setBranchId(branchId);
                userList.setBranch(branch);
                userList.setDepartmentId(departmentId);
                userList.setDepartment(department);
                userList.setIsStopped(isStopped);

                //userList.setCreated_date(createdDate);

                ret.add(userList);

                System.out.println(isStopped);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


}
