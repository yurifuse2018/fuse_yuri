package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter7.beans.Comment;
import chapter7.exception.SQLRuntimeException;

public class DeleteCommentDao {

	public void delete(Connection connection,int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("delete from comments where comments.id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1,id);

			ps.executeUpdate();

		}catch(SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
}