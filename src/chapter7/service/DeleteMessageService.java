package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;

import chapter7.dao.DeleteMessageDao;


public class DeleteMessageService {

	public void register(int id) {
        Connection connection = null;
        try {
            connection = getConnection();

            DeleteMessageDao messageDao = new DeleteMessageDao();
            messageDao.delete(connection,id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

	}
}