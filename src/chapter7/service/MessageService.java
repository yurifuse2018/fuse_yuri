package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import chapter7.beans.Message;
import chapter7.beans.UserMessage;
import chapter7.dao.MessageDao;
import chapter7.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {
        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;




    public List<UserMessage> getMessage(String sDate,String eDate,String category) {
        Connection connection = null;

        try {
            connection = getConnection();

            if(sDate==null || sDate=="") {
            	sDate = "2018/01/01";
            }
            if(eDate==null || eDate=="") {
            	Date date = new Date();
            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            	eDate =sdf.format(date);
            }

            System.out.println(sDate);
            System.out.println(eDate);
            System.out.println(category);

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection,LIMIT_NUM,sDate,eDate,category);


            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}