package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.User;
import chapter7.beans.UserList;
import chapter7.dao.UserDao;
import chapter7.dao.UserSettingDao;
import chapter7.utils.CipherUtil;

public class UserService {



    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



    private static final int LIMIT_NUM = 1000;

    public List<UserList> getUserList() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserSettingDao userSettingDao = new UserSettingDao();
            List<UserList> ret = userSettingDao.getUserList(connection,LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public boolean getAccountList(String account) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();

    		return userDao.getAccount(connection,account);

    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public boolean getAccountList(String account,int userId) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		return userDao.getAccount(connection,account, userId);

    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public boolean getUserId(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            UserDao userDao = new UserDao();
            List<Integer> ret = userDao.getUserId(connection);
            commit(connection);

            if(ret.contains(userId)) {
            	return true;
            }else {
            	return false;
            }

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}