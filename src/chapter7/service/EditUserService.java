package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.User;
import chapter7.beans.UserList;
import chapter7.dao.EditUserDao;
import chapter7.dao.UserSettingDao;
import chapter7.utils.CipherUtil;

public class EditUserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            EditUserDao editUserDao = new EditUserDao();
            editUserDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


private static final int LIMIT_NUM = 1000;

    public List<UserList> getUserList() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserSettingDao userSettingDao = new UserSettingDao();
            List<UserList> ret = userSettingDao.getUserList(connection,LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //EditUserServletから編集後のデータをupdate
    public void update(User editUser) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(editUser.getPassword());
            editUser.setPassword(encPassword);

            EditUserDao editUserDao = new EditUserDao();
            editUserDao.update(connection,editUser);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User getEditUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            EditUserDao editUserDao = new EditUserDao();
            User user = editUserDao.getUserList(connection, userId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public void stopUser(int id, int stop) {

        Connection connection = null;
        try {
            connection = getConnection();

            EditUserDao editUserDao = new EditUserDao();
            editUserDao.stopUser(connection,id,stop);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
