package chapter7.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{


	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws java.io.IOException, ServletException {

		      HttpSession session = ((HttpServletRequest) request).getSession();

			  User loginUser = (User) session.getAttribute("loginUser");
			  String servlet = ((HttpServletRequest) request).getServletPath();
			  String login = "/login";
			  String logout = "/logout";
			  String css = "/css/login.css";

			  System.out.println(servlet);


		      if(loginUser==null) {

		    	  if(!servlet.equals(css)&&!servlet.equals(login)&&!servlet.equals(logout)) {
		    		  String message = "ログインしてください。";
			    	  session.setAttribute("errorMessages",message);
			    	  ((HttpServletResponse) response).sendRedirect("login");
			    	  return;
		    	  }
		      }


			    if(loginUser!=null) {
					int branchId = loginUser.getBranchId();
					int departmentId = loginUser.getDepartmentId();

					 if(departmentId != 1) {
						  String top = "/index.jsp";
						  String newMessage = "/newMessage";
						  String newComment = "/newComment";
						  String deleteComment = "/deleteComment";
						  String deleteMessage = "/deleteMessage";
						  String style = "/css/style.css";
						  String style3 = "/css/style3.css";
//						  String signup = "/signup";
//						  String userSetting = "/userSetting";
//						  String editUser = "/editUser";

				    	  if(!(servlet.equals(top)||servlet.equals(newMessage)||servlet.equals(newComment)||servlet.equals(deleteComment)||servlet.equals(deleteMessage)||servlet.equals(logout)||servlet.equals(style)||servlet.equals(style3))) {
				    		  String message = "権限がありません。";
				    		  session.setAttribute("errorMessages", message);
				    		  ((HttpServletResponse) response).sendRedirect("./");
				    		  return;
				    	  }
//				    	  else if(servlet.equals(userSetting)) {
//				    		  System.out.println(userSetting);
//				    		  ((HttpServletResponse) response).sendRedirect("./");
//				    		  return;
//				    	  }
//				    	  else if(servlet.equals(editUser)) {
//				    		  System.out.println(editUser);
//				    		  ((HttpServletResponse) response).sendRedirect("./");
//				    		  return;
//				    	  }else if(servlet.equals(editUser)) {
//				    		  System.out.println(editUser);
//				    		  ((HttpServletResponse) response).sendRedirect("./");
//				    		  return;
//				    	  }
					  }
			    }
			    chain.doFilter(request, response);

		  }

	public void init(FilterConfig filterConfig) throws ServletException {

	}



	public void destroy() {

	   }




}
