package chapter7.filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;



//@WebFilter(urlPatterns = {"/userSetting","/signup","/editUser"})
public class LoginFilterAuth {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws java.io.IOException, ServletException {

		      HttpSession session = ((HttpServletRequest) request).getSession();

			  User loginUser = (User) session.getAttribute("loginUser");
			  String servlet = ((HttpServletRequest) request).getServletPath();
			  String path = "/login";



			  if(loginUser!=null) {
					int branchId = loginUser.getBranchId();
					int departmentId = loginUser.getDepartmentId();
					 System.out.println(branchId);
					 System.out.println(departmentId);

					  if(branchId != 4 && departmentId != 1) {
				    	  System.out.println(2011111111);
						  String signup = "/signup";
						  String userSetting = "/userSetting";
						  String editUser = "/editUser";

				    	  if(servlet.equals(signup)) {
				    		  System.out.println(signup);
				    		  ((HttpServletResponse) response).sendRedirect("./");
				    	  }
				    	  else if(servlet.equals(userSetting)) {
				    		  System.out.println(userSetting);
				    		  ((HttpServletResponse) response).sendRedirect("./");
				    	  }
				    	  else if(servlet.equals(editUser)) {
				    		  System.out.println(editUser);
				    		  ((HttpServletResponse) response).sendRedirect("./");
				    	  }

					  }
					  chain.doFilter(request, response);
			    }

			}
}
